# STL-sango-case

## Informations

/////////////////////////////////////

:earth_africa: :fr:

Boîtier principal MyStation (Sango).

X partie(s):
- [Principale](./parts/SANGO000-main/)
- ...

/////////////////////////////////////

:earth_americas: :us:

Main MyStation case (Sango).

X part(s):
- [Main](./parts/SANGO000-main/)
- ...

/////////////////////////////////////

## Platforms

- [Thingiverse](https://www.thingiverse.com/thing:XXXXXXXXX)

## Licence

©© Creative Commons - Attribution - NonCommercial - ShareAlike

[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)
